provider "aws" {
        profile = "default"
}

module "s3" {
    source = "/home/ec2-user/s3"
    #bucket name should be unique
    bucket_name = "encore-tvm-demo-bucket"
}
