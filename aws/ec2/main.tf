provider "aws" {
  profile             = "test"
}

resource "aws_instance" "ec2" {
  ami                         = "ami-0cff7528ff583bf9a"
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  vpc_security_group_ids      = ["sg-0b3b118b91fc916db"]
  key_name                    = "sunil-jumphost"
  tags = {
    Name = "Test EC2 Instance"
  }
}
